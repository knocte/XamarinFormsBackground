## Background library for Xamarin.Forms, compatible with Android, iOS, Windows, Linux & Mac

This is a simple library to abstract background tasks in the various platforms that Xamarin.Forms supports.

### Setup
* Available on NuGet: https://www.nuget.org/packages/Xamarin.Forms.Background/
* Install into your .NET Standard libraries and Client projects.


### Supports
* Xamarin.Android
* Xamarin.iOS
* Xamarin.Mac
* Windows (UWP&WPF)
* Linux

### API Usage

TBD

```csharp
using Plugin.Background;

...

//To set the clipboard
...
```
