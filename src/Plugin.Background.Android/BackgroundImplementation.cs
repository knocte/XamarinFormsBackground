using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Plugin.Background.Abstractions;
using System;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

namespace Plugin.Background
{
    /// <summary>
    /// Implementation for Feature
    /// </summary>
    public class BackgroundImplementation : IBackground
    {
        static FormsApplicationActivity staticMainActivity = null;
        
        static void Init(FormsApplicationActivity mainActivity)
        {
            staticMainActivity = mainActivity;
        }

        public void Start<T>(Func<Task<T>> taskCreator) where T : class
        {
            var service = new BackgroundImplementationService<T>(taskCreator);
            if (staticMainActivity == null)
                throw new InvalidOperationException("Did you forget to init Plugin.Background in your platform project?");
            var intent = new Intent(service, service.GetType());
            staticMainActivity.StartService(intent);
        }
    }

    [Service]
    public class BackgroundImplementationService<T> : Service where T : class
    {
        public BackgroundImplementationService(Func<Task<T>> taskCreator)
        {
            this.taskCreator = taskCreator;
        }

        CancellationTokenSource _cts;
        Func<Task<T>> taskCreator;

        public override IBinder OnBind(Intent intent)
        {
            throw new NotImplementedException();
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            _cts = new CancellationTokenSource();

            Task.Run(() =>
            {
                try
                {
                    var task = taskCreator();
                    task.Wait();
                    MessagingCenter.Send<T>(task.Result, task.GetHashCode().ToString());
                }
                catch (System.OperationCanceledException)
                {
                }
                finally
                {
                    //if
                }
            }, _cts.Token);

            return StartCommandResult.Sticky;
        }
    }
}