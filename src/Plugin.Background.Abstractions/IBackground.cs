﻿using System;
using System.Threading.Tasks;

namespace Plugin.Background.Abstractions
{
    /// <summary>
    /// Interface for Background
    /// </summary>
    public interface IBackground
    {
        void Start<T>(Func<Task<T>> taskCreator) where T : class;
    }
}
