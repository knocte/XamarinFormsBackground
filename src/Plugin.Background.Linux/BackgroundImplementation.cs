﻿using Plugin.Background.Abstractions;
using System;
using System.Threading.Tasks;

namespace Plugin.Background
{
    /// <summary>
    /// Implementation for Background
    /// </summary>
    public class BackgroundImplementation : IBackground
    {
        public void Start<T>(Func<Task<T>> taskCreator) where T : class
        {
        }
    }
}