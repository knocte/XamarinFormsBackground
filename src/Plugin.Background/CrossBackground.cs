﻿using Plugin.Background.Abstractions;
using System;

namespace Plugin.Background
{
    /// <summary>
    /// Cross platform Clipboard implemenations
    /// </summary>
    public class CrossBackground
    {
        static Lazy<IBackground> Implementation = new Lazy<IBackground>(() => CreateClipboard(), System.Threading.LazyThreadSafetyMode.PublicationOnly);

        /// <summary>
        /// Current settings to use
        /// </summary>
        public static IBackground Current
        {
            get
            {
                var ret = Implementation.Value;
                if (ret == null)
                {
                    throw NotImplementedInReferenceAssembly();
                }
                return ret;
            }
            set
            {
                Implementation = new Lazy<IBackground>(() => value, System.Threading.LazyThreadSafetyMode.PublicationOnly);
            }
        }

        static IBackground CreateClipboard()
        {
#if NETSTANDARD1_0
            return null;
#else
            return new BackgroundImplementation();
#endif
        }

        internal static Exception NotImplementedInReferenceAssembly()
        {
            return new NotImplementedException("This functionality is not implemented in the portable version of this assembly.  You should reference the NuGet package from your main application project in order to reference the platform-specific implementation.");
        }
    }
}
